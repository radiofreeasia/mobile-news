# nuus

**Development Domain**: https://rfa-anynews-dev.gpcmdln.net/

That domain runs code from the develop branch with the full censorship circumvention apparatus.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
